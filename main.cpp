#include <iostream>
#include <SDL2/SDL.h>
#include "graphics/graphics.h"

int main() {
  using namespace mintcookie;
  using namespace graphics;
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    std::cout << "Unable to initialize SDL\n";
    return 1;
  }

  Window window("MintCookie", 800, 800);
  Shape line("line", 300, 240, 150, 10, window.renderer);

  SDL_Event e;
  bool is_running = true;
  while(is_running) {
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        is_running = false;
      }
      window.update();
    }
  }

  SDL_Quit();
  return 0;
}
