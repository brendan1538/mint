#include <iostream>
#include <SDL2/SDL.h>
#include "graphics.h"

namespace mintcookie { namespace graphics {
  Window::Window(const char* title, int width, int height) {
    m_Title = title;
    m_Width = width;
    m_Height = height;

    init();
  }

  Window::~Window() {}

  void Window::init() {
    window = SDL_CreateWindow(
      m_Title,
      0,
      0,
      m_Width,
      m_Height,
      SDL_WINDOW_SHOWN
    );
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (window == NULL) {
      std::cout << "Problem creating window with SDL: " << SDL_GetError() << std::endl;
    }
  }

  void Window::update() const {
    // issa triangle
    // SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    // SDL_RenderClear(renderer);
    // SDL_RenderPresent(renderer);

    SDL_Delay(16);
  }
} }
