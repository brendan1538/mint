#pragma once

#include <SDL2/SDL.h>

namespace mintcookie { namespace graphics {
  class Window {
    public:
      const char *m_Title;
      int m_Width;
      int m_Height;
      SDL_Window *window;
      SDL_Renderer *renderer;
    public: 
      Window(const char *title, int width, int height);
      ~Window();
      void update() const;
      void closed() const;
    private:
      void init();
  };
  class Shape {
    public:
      const char *m_Type;
      int m_X;
      int m_Y;
      int m_Width;
      int m_Height;
      SDL_Renderer *m_Renderer;
    public:
      Shape(const char *type, int x, int y, int width, int height, SDL_Renderer *renderer);
      ~Shape();
    private:
      void init();
      int** circle();
  };
} }
