#include <iostream>
#include <SDL2/SDL.h>
#include "graphics.h"

namespace mintcookie { namespace graphics {
  Shape::Shape(const char *type, int x, int y, int width, int height, SDL_Renderer *renderer) {
    m_Type = type;
    m_X = x;
    m_Y = y;
    m_Width = width;
    m_Height = height;
    m_Renderer = renderer;
    init();
  }

  Shape::~Shape() {}

  void Shape::init() {
    SDL_RenderClear(m_Renderer);
    SDL_SetRenderDrawColor(m_Renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLine(m_Renderer, m_X, m_Y, (m_X + m_Width), m_Y);
    SDL_RenderPresent(m_Renderer);
  }

}}