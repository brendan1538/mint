CC := /usr/bin/g++
CFLAGS := -I /usr/include/SDL2 -L lib -lSDL2

SRCS := graphics/window.cpp graphics/shape.cpp

OBJS := $(SRCS:.cpp=.o)

MAIN := engine
all: $(MAIN)

$(MAIN): $(OBJS)
	if [ ! -d out/ ]; then mkdir out; fi
	$(CC) main.cpp $(OBJS) -o out/$(MAIN) $(CFLAGS)

clean:
	if [[ -f out/$(MAIN) ]]; then rm out/$(MAIN); fi
